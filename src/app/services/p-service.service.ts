import { PData } from './../interfaces/p-data';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PServiceService {

  /**
   * Arreglo de datos (TEMPORALMENTE HARDCODEADOS)
   */
  lista: PData[] = [
    {
      id: '001',
      cantPersonas: 1,
      cargo: 'Ingeniero de Sistemas',
      enfasis: 'Desarrollo Web',
      motivo: 'otro',
      fechaStart: new Date(12 / 12 / 1995),
      fechaEnd: new Date(12 / 1 / 1996),
      observaciones: 'Otro',
      educacion: 'Pregrado',
      formacion: 'uno',
      experiencia: '2 años',
      desistimiento: true,
    },
    {
      id: '002',
      cantPersonas: 1,
      cargo: 'Medico general',
      enfasis: 'General',
      motivo: 'otro',
      fechaStart: new Date(12 / 12 / 1995),
      fechaEnd: new Date(12 / 1 / 1996),
      observaciones: 'Otro',
      educacion: 'Pregrado',
      formacion: 'uno',
      experiencia: '1 años',
      desistimiento: true,
    },
    {
      id: '003',
      cantPersonas: 1,
      cargo: 'Medico general',
      enfasis: 'General',
      motivo: 'otro',
      fechaStart: new Date(12 / 12 / 1995),
      fechaEnd: new Date(12 / 1 / 1996),
      observaciones: 'Otro',
      educacion: 'Pregrado',
      formacion: 'uno',
      experiencia: '1 años',
      desistimiento: false,
    },
    {
      id: '004',
      cantPersonas: 5,
      cargo: 'Auxiliar Enfermeria',
      enfasis: 'Enfermeria',
      motivo: 'Muchos Pacientes',
      fechaStart: new Date(12 / 12 / 1995),
      fechaEnd: new Date(12 / 1 / 1996),
      observaciones: 'Buen aseo',
      educacion: 'Pregrado',
      formacion: 'dos',
      experiencia: '1 años',
      desistimiento: false,
    }
  ];

  constructor() { }

  /**
   * Método encargado de traer los datos de la lista
   */
  getData(): any{
    return this.lista.slice();
  }
}
