import { PServiceService } from './../../services/p-service.service';
import { PData, Accion } from './../../interfaces/p-data';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.scss'],
})
export class TablaComponent implements OnInit, AfterViewInit {
  /**
   * Arreglo de datos, que contiene la definición de la columna y el título de la misma
   * para ser asignadas en la tabla
   * Arreglo del menú de acciones para ser mostrados en la tabla
   */
  acciones: Accion[] = [
    { columnDef: 'id', header: 'ID', fixed: 'true' },
    { columnDef: 'cargo', header: 'Cargo' },
    { columnDef: 'cantPersonas', header: 'Cantidad de Personas' },
    { columnDef: 'enfasis', header: 'Enfasis' },
    { columnDef: 'motivo', header: 'Motivo' },
    { columnDef: 'observaciones', header: 'Observaciones' },
    { columnDef: 'educacion', header: 'Educacion' },
    { columnDef: 'formacion', header: 'Formacion' },
    { columnDef: 'experiencia', header: 'Experiencia' },
    { columnDef: 'desistimiento', header: 'Desistimiento' },
    {
      columnDef: 'action',
      header: 'Acciones',
      actions: true,
    },
  ];

  /**
   * Arreglo que contiene las columnas a mostrar en la tabla
   */
  desplegarColumnas = [
    'id',
    'cargo',
    'cantPersonas',
    'enfasis',
    'motivo',
    'observaciones',
    'educacion',
    'formacion',
    'experiencia',
    'desistimiento',
    'action',
  ];

  /**
   * variable encargada de guardar los datos traídos por el servicio
   */
  dataSource = new MatTableDataSource<PData>();

  /**
   * Variable que inicializa el ordenador de datos por columna
   */
  @ViewChild(MatSort) ordenamiento: MatSort;

  /**
   * Variable que inicializa el paginador de la tabla
   */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
   * Constructor del componente, el cual inicializa el servicio y el modal para su respectivo uso
   * @param pServiceService Servicio en donde se ejecutan las funciones CRUD (Listar, Crear, Actualizar, Borrar)
   * @param dialog Componente MODAL, encargado de mostrar un formulario
   */
  constructor(
    private pServiceService: PServiceService,
    public dialog: MatDialog
  ) {}

  /**
   * Método encargada de ejecutar diferentes acciones al momento de ejecutar el componente
   * Método getData(), encargada de guardar los datos que son traídos desde el servicio
   */
  ngOnInit(): void {
    this.dataSource.data = this.pServiceService.getData();
  }

  /**
   * Método encargada de ejecutar diferentes acciones después de ejecutarse el componente
   * Inicializa el atributo para ordenar las columnas de la tabla
   * Inicializa el atributo para paginar los datos de la tabla
   */
  ngAfterViewInit(): void {
    this.dataSource.sort = this.ordenamiento;
    this.dataSource.paginator = this.paginator;
  }

  /**
   * Método encargado de ejecutar el filtrado (búsqueda) de datos en la tabla
   * @param filtro Dato recibido como parámetro para la búsqueda
   */
  ngHacerFiltro(filtro: string): void {
    this.dataSource.filter = filtro;
  }

  /**
   * Método encargado de lanzar el modal
   */
  ngOpenDialog(): void {
    this.dialog.open(ModalComponent);
  }

  /**
   * Método encargado de lanzar una acción al dar clic en los botones del menú de acciones de la tabla
   */
  ngClick(): void {
    console.log('Se hizo clic a un boton');
  }
}
