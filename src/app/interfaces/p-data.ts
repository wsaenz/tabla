/**
 * Interfaz encargada de asignar el tipo a cada uno de los campos de datos a traer desde el Servicio
 */
export interface PData {
  id: string;
  cantPersonas: number;
  cargo: string;
  enfasis: string;
  motivo: string;
  fechaStart?: Date;
  fechaEnd?: Date;
  observaciones: string;
  educacion: string;
  formacion: string;
  experiencia: string;
  desistimiento: boolean;
}


/**
 * Interface encargada de asignar el tipo de dato a cada uno de los datos a mostrar en el menú de acciones
 */
export interface Accion {
  columnDef: string;
  header: string;
  fixed?: string;
  actions?: boolean;
}
